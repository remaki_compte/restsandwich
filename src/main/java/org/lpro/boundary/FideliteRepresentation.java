package org.lpro.boundary;


import control.KeyManagement;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.net.URI;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.lpro.boundary.FideliteResource;
import org.lpro.entity.Commande;
import org.lpro.entity.Fidelite;
import org.mindrot.jbcrypt.BCrypt;
import provider.Secured;

@Path("/fidelite")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class FideliteRepresentation {

    @EJB
    FideliteResource fideliteResource;
    
    @EJB
    CommandeResource CommandeResource;
      
    @Inject
    private KeyManagement keyManagement;
    
    @Context
    private UriInfo uriInfo;
    
    
    @POST
    public Response addCarte(Fidelite c_fi, @Context UriInfo uriInfo) {
        Fidelite fid = this.fideliteResource.save(c_fi);
        URI uri = uriInfo.getAbsolutePathBuilder().path(fid.getId()).build();
        String carte_id = fid.getCarte();
        Map map = Collections.singletonMap("Identifiant de carte", carte_id );
        return Response.created(uri)
                .entity(map)
                .build();
    }
    
    @POST
    @Path("/auth")
    public Response getCard(@Context HttpHeaders headers , @Context UriInfo uriInfo ) throws Exception
    {
        
        String auth = headers.getRequestHeader("Authorization").get(0);
        
        // Autoriser uniquement l'authentification BASIC
        if( auth == null || !auth.toUpperCase().startsWith("BASIC ") )
            throw new NotAuthorizedException ("Problème d'authentification");
        
        //On décrypte l'en-tete Authorization
        String base64Credentials = auth.substring("Basic".length()).trim();
        byte[] decodedStr = Base64.getDecoder().decode( base64Credentials );
        String auth_info = new String( decodedStr, "utf-8" );
        String[] parts = auth_info.split(":");
        
        // On récupére l'ID de la carte
        String card =  parts[0] ;
        
        // On récupére le mot de passe
        String mdp = parts[1] ;
        
  
        try 
        {  
            Fidelite fid = this.fideliteResource.findByCard(card);
            URI uri = uriInfo.getAbsolutePathBuilder().path(fid.getId()).build();

            // On récupére l'ID de la carte de fidélité
            String idCarteFidelite =  fid.getId();

            //Authentification avec la carte de fidélité
            register(card,mdp);

            // On associe un token à cette carte de fidélité
            String token = issueToken(idCarteFidelite);

            // On récupére la cagnotte de la carte de fidélité
            double cagnottte = fid.getCagnotte();


            // Création d'une liste avec la cagnotte et le token
            Map can = Collections.singletonMap("Cagnotte", cagnottte );
            Map tok = Collections.singletonMap("Token", token );
            List<Map> list = new ArrayList<>();
            list.add(can);
            list.add(tok); 

            return Response.ok()
                    .entity(list)
                    .build();
        }
        catch (Exception e) {
            
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

    }
    
    
    @Secured
    @Path("/paiement/{commandeId}/")
    @POST
    public Response PayWithCard(@PathParam("commandeId") String commandeId, @Context UriInfo uriInfo,
    @DefaultValue("true") @QueryParam("reduction") String isReduc,
    @Context HttpHeaders httpHeaders
    )
    {        
        // Décrypte et Passe le token à l'entité sinon genere une exception
        String id_f  = httpHeaders.getHeaderString("data") ;
        Fidelite fid = this.fideliteResource.findById(id_f);
        Commande commande = this.CommandeResource.findById(commandeId);
        
        // On récupére le montant de la cagnotte
        double cagnotte = fid.getCagnotte();
        // On récupére le montant de la commande
        double montant_commande = commande.calculateMontant();
        
        // Si la réduction est souhaitée et que le montant de la cagnote > 100
        if( "true".equals(isReduc) &&  fid.ReductionIsOk() && !"paid".equals(commande.getEtat()))
        { 
            commande.setMontant(fid.MontantApresReduction(montant_commande) );
            fid.setCagnotte(0);
            JsonObject jsonResult = Json.createObjectBuilder()
            .add("invoice","paid").build();
            return Response.ok().entity(jsonResult).build();
        }
        // Si la réduction n'est pas souhaitée on crédite la cagnotte du montant de la commande
        else if ( "false".equals(isReduc) || !fid.ReductionIsOk() && !"paid".equals(commande.getEtat()) )
        {
            fid.setCagnotte(cagnotte + montant_commande );
            commande.setEtat("paid");           
            JsonObject jsonResult = Json.createObjectBuilder().add("Nouvelle cagnotte",  fid.getCagnotte() )
                    .add("invoice","paid").build();
            return Response.ok().entity(jsonResult).build();
        }
        
        else
        {
            throw new NotAuthorizedException ("Données Incorrecte");
        }
           
    }
   
    private void register(String id_carte, String mdp) throws Exception {
        
 
        Fidelite fid = this.fideliteResource.findByCard(id_carte);
        String mdpBD = fid.getPassword();
        
        if(!BCrypt.checkpw(mdp,mdpBD)) 
        {     
            throw new NotAuthorizedException ("Problème d'authentification");
        }
    }
    
    
    
     private String issueToken(String FideliteId) {
        Key key = keyManagement.generateKey();
        String jwtToken = Jwts.builder()
                .setSubject(FideliteId)
                .setIssuer(uriInfo.getAbsolutePath().toString())
                .setIssuedAt(new Date())
                .setExpiration(toDate(LocalDateTime.now().plusMinutes(5L)))
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
        System.out.println(">>>> token/key : " + jwtToken + " - " + key);
        return jwtToken;
    }
    
 
    private Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }


}
