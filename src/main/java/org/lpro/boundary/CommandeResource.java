/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lpro.boundary;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.CacheStoreMode;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Path;
import org.lpro.entity.Categorie;

import org.lpro.entity.Commande;
import org.lpro.entity.Ingredients;

import org.lpro.entity.Sandwich;

/**
 *
 * @author remaki
 */
@Path("commandes")
@Stateless
public class CommandeResource {
    
    
     @PersistenceContext
    EntityManager em;
     
      @EJB
    SandwichResource SandwichResource;

    public Commande findById(String id) {
        
         return this.em.find(Commande.class, id);
      
    }

    public List<Commande> findAll() {
        Query q = this.em.createNamedQuery("Commande.findAll", Commande.class);
        // pour éviter les pbs de cache
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return q.getResultList();
    }
    
    
    public List<Commande> findAllCommandesPaginated(int offset, int limit) {
        Query q = this.em.createNamedQuery("Commande.findAll", Commande.class);
        // pour éviter les pbs de cache
        List<Commande> list= this.findAll();
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return list.subList(offset, limit);
    }
    
    public List<Commande> findByDateLivraison() {
        
        Query q = this.em.createNamedQuery("Commande.findByDateLivraison", Commande.class);
        // pour éviter les pbs de cache
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return q.getResultList();
        
    }
    
    public List<Commande> findByDateCreation() {
        
        Query q = this.em.createNamedQuery("Commande.findByDateCreation", Commande.class);
        // pour éviter les pbs de cache
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return q.getResultList();
        
    }
    
    
     public List<Commande> findByEtatAndDateLivraison(String etat,Date date) {
        Query query = em.createQuery("SELECT c FROM Commande c where c.dateLivraison= :date or c.etat = :etat");
        
        query.setParameter("date", date);
        query.setParameter("etat", etat);
        List<Commande> liste = query.getResultList();
        return liste;
    }
    
    public Commande save(Commande com) {
         
         Commande commande = new Commande(com.getNbre(),com.getDateLivraison());
         commande.setId(UUID.randomUUID().toString());
         
         commande.setSandwichs(new ArrayList<>());       
         commande.calculateMontant();
        
        return this.em.merge(commande);
    }
    
    public Sandwich updateCommande (String id, Commande commande, String sandwichId,Sandwich sandwich) {
        
        Commande ref = this.em.getReference(Commande.class, id);
        
        try {         
                 sandwich.calculatePrix();
                 ref.getSandwich(sandwichId).setIngredients(sandwich.getIngredients());
                ref.setSandwich(sandwichId, sandwich);
                
                ref = this.em.merge(ref);
       
        }
        catch(EntityNotFoundException e) {
            
            e.getMessage();
        }
        
        
        return ref.getSandwich(sandwichId);
        
    }
    
    
     public Commande updateCommandeDate (String id, Commande commande) {
        
        Commande ref = this.em.getReference(Commande.class, id);
        
        try {         
                 
        
                ref.setDateLivraison(commande.getDateLivraison());
                
                ref = this.em.merge(ref);
       
        }
        catch(EntityNotFoundException e) {
            
            e.getMessage();
        }
        
        
        return ref;
        
    }
     
     public Commande updateEtatCommande (String id, String etat) {
        
        Commande ref = this.em.getReference(Commande.class, id);
        
        try {         
                 
        
                ref.setEtat(etat);
                
                 this.em.persist(ref);
       
        }
        catch(EntityNotFoundException e) {
            
            e.getMessage();
        }
        
        
        return ref;
        
    }

    public void delete(String id) {
        try {
            Commande ref = this.em.getReference(Commande.class, id);
            this.em.remove(ref);
        } catch (EntityNotFoundException e) {
            // on veut supprimer, et elle n'existe pas, donc c'est bon
        }
    } 
    
    
    public void supprimerSandwich(String commandeId, String sandwichId) {
         try {
            Commande ref = this.em.getReference(Commande.class, commandeId);
            Sandwich sandwich = this.em.getReference(Sandwich.class, sandwichId);
            this.em.remove(sandwich);
           
            
          
        } catch (EntityNotFoundException e) {
            // on veut supprimer, et elle n'existe pas, donc c'est bon
        }
        
        
    }
    
    
    public Long countAll() {
        return (long) em.createNamedQuery("Commande.findAll", Commande.class)
                .setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH)
                .getResultList()
                .size();
}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
