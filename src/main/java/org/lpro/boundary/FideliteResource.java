
package org.lpro.boundary;

import control.PasswordManagement;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Path;
import org.lpro.entity.Fidelite;

@Path("fidelite")
@Stateless
public class FideliteResource {
    
    
    @PersistenceContext
    EntityManager em;

    public Fidelite findById(String id) {
        return this.em.find(Fidelite.class, id);
    }
    
    public Fidelite save(Fidelite fid) {
        fid.setId(UUID.randomUUID().toString());
        fid.setCarte(UUID.randomUUID().toString());
        fid.setPassword(PasswordManagement.digestPassword(fid.getPassword()));
        return this.em.merge(fid);
    }


     public Fidelite findByCard(String id_carte) {
        Query query;
        query = em.createQuery("SELECT c FROM Fidelite c where c.id_carte= :id ");
        query.setParameter("id", id_carte);
        Fidelite fa = (Fidelite) query.getSingleResult();
        return fa;
    }
     
     public Fidelite updateFidelite (String id , double nouvelle_cagnotte) {
        
        Fidelite ref;
        try {
             ref = this.em.getReference(Fidelite.class, id);
             ref.setCagnotte(nouvelle_cagnotte);
             return this.em.merge(ref);
        }
        catch(EntityNotFoundException e) {
            return null;
        }  
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
