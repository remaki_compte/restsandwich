package org.lpro.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class Fidelite implements Serializable {
    
    
    private static final long serialVersionUID = 1L;

    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    
    // Mot de passe
    private String password;

    // Adresse
    private String adresse;
    
    // Identifiant de la carte de fidélité
    private String id_carte;
    
    // Cagnote de la carte
    private double cagnotte ;
    
    // Seuil de réudction
    final private static int SEUIL_REDUCTION = 100 ;
    
    //Pourcentage de réduction
   final private static double REDUCTION = 5/100 ; 
    
    @XmlElement(name="_links")
    @Transient 
    private List<Link>  links = new ArrayList<>();
    
    public List<Link> getLinks() {
        
        return links;
    }
    
    
    public void addLink(String uri, String rel) {
        
        this.links.add(new Link(rel, uri));
    }

    //constructeur vide pour JPA 
    public Fidelite() {

    }
    
    public Fidelite(String adresse, String password) {

        this.cagnotte = 0;
        this.adresse = adresse;
        this.password = password;
        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
    //Getters et setters 

    public String getCarte() {
        return id_carte;
    }
    
    public void setCarte(String id_carte){
        this.id_carte = id_carte;
    }
         
    public double getCagnotte() {
        return cagnotte;
    }

    public void setCagnotte(double cagnotte) {
        this.cagnotte = cagnotte;
    }
    
    public void setPassword(String password)
    {
        this.password = password;
    }
    
    public String getPassword()
    {
        return password;
    }
    
    public boolean ReductionIsOk()
    {
        return cagnotte > SEUIL_REDUCTION ;
    }
    
    public double MontantApresReduction(double montant_commande)
    {
       return montant_commande - ( montant_commande * REDUCTION ); 
    }
    


}
