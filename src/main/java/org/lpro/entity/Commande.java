/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lpro.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author remaki
 */

@Entity  //pour dire qu'on ça va persister dans une table dans une base de données
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@NamedQueries({
    @NamedQuery(name="Commande.findAll",query="SELECT i from Commande i"),
    
    @NamedQuery(name="Commande.findByDateLivraison",query="SELECT c from Commande c ORDER BY c.dateLivraison DESC"),
    @NamedQuery(name="Commande.findByDateCreation",query="SELECT c from Commande c ORDER BY c.dateCreation DESC"),
    @NamedQuery(name="Commande.findByEtatAndDate",query="SELECT c from Commande c where c.dateLivraison = :date and c.etat= :etat")      
})
 //ma classe accepte une requete de ce type,on peut créer d'autres
public class Commande implements Serializable{
        private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id;
   
    private int nbre=0;
    //Date date = new Date();

    //DateFormat shortDateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT);
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd/MM/yyyy")
    private Date dateLivraison;  
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd/MM/yyyy")
    private Date dateCreation;
    public final static String [] etats = {"CREATED","PAID","NOT-PAID","PENDING","READY","DELIVERED"}; //la commande peut être: créée (created), payée (paid), en cours (pending), prête (ready), livrée (delivered)
    private String etat;
    private double montant;
    private String paiement;
   
    
     @OneToMany(cascade = CascadeType.ALL, mappedBy = "commande",fetch=FetchType.EAGER)  // une instance de la classe categorie correspond plusieurs instance de ingredients 
    @JsonManagedReference  //le point d'entrée  (pour eviter le cycle)
     
    private List<Sandwich> sandwichs; //collection
    @XmlElement(name="_links")
    @Transient 
    private List<Link>  links = new ArrayList<>();
    
    public List<Link> getLinks() {
        
        return links;
    }
    
    
    public void addLink(String uri, String rel) {
        
        this.links.add(new Link(rel, uri));
    }

    //constructeur vide pour JPA 
    public Commande() {
        this.sandwichs = new ArrayList<>();
        
        //JPA
    }
    
    

    public Commande(int nbre, Date date) {
        
         
        this.nbre = nbre;
        this.dateCreation = toDate(LocalDateTime.now());
        this.dateLivraison = date;
        this.sandwichs = new ArrayList<>();
        this.etat =this.etats[0];
        this.paiement = this.etats[2];
        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
    //Getters et setters 

    public int getNbre() {
        return nbre;
    }

    public void setNbre(int nbre) {
        this.nbre = nbre;
    }

  

    public Date getDateLivraison() {
        return dateLivraison;
    }

    public void setDateLivraison(Date dateLivraison) {
        this.dateLivraison = dateLivraison;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getPaiement() {
        return paiement;
    }

    public void setPaiement(String paiement) {
        this.paiement = paiement;
    }

    
    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public List<Sandwich> getSandwichs() {
        return sandwichs;
    }

    public void setSandwichs(List<Sandwich> sandwichs) {
        this.sandwichs = sandwichs;
    }
    
    public Sandwich getSandwich(String sandwichId){
        List <Sandwich> lsan = this.getSandwichs();
        Sandwich res=null;
            for(Sandwich in : lsan) {

               if(in.getId().equals(sandwichId)) {

                  res = in;
                }

             

           }
            
        return res;
    }
    
    public void setSandwich(String sandwichId, Sandwich sandwich){
        List <Sandwich> lsan = this.getSandwichs();
       
            for(Sandwich in : lsan) {

                    if(in.getId().equals(sandwichId)) {

                       in.setTaille(sandwich.getTaille());
                       in.setType(sandwich.getType());
                    }
                    this.setSandwichs(lsan);                
            
           }          
            
        
    }
    
    public double calculateMontant() {
        this.montant = 0;
        List<Sandwich> lsan= this.getSandwichs();
        
        if(lsan != null) {
            this.nbre = lsan.size();
        for(Sandwich in : lsan) {
                
          this.montant+= in.calculatePrix();
                
            
                
            }
        }
        
        return this.montant;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

   
  
    
     public Date StringtoDate(String s) {
        Date date = null;
        SimpleDateFormat inputDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        inputDateFormat.setTimeZone(TimeZone.getDefault());
        //Date dateMax = Date.from(LocalDateTime.now()
               // .plusMinutes(10)
               // .atZone(ZoneId.systemDefault())
                //.toInstant());

       // String dateString = inputDateFormat.format(dateMax);


       try {
            date = inputDateFormat.parse(s);
            //dateMax = inputDateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //if (date != null) {
           // if (!(date.compareTo(dateMax) > 0))
            //   date = null;
       // }

        return date;
}
     
     
    
 public String verifierPaiement() {


    return this.paiement;
 }

     private Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
    
}


