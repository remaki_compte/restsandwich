# README # Projet -Le BonSandwich


This README would normally document whatever steps are necessary to get your application up and running.

### Directives d'installation###

* Quick summary
* Version 1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
Docker
Recupérer les 3 fichiers :
(Dockerfile, docker-entrypoint.sh postgresql-9.4.1212.jar)
* docker build -t postgres . 
* docker start bd-postgres

###Postgres
Création d'un User pour Postgres:
<br/>  • docker run --name bd-postgres -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=riovas -p 5432:5432 -d postgres

Run la BD-Postgres:
<br/>  • docker run -it --rm --link bd-postgres:postgres postgres psql -h bd-postgres -U postgres

Import de la BD postgres:
<br/>  • psql -U postgres bd-postgres < database.sql

###Wildfly
Ajoute du Driver postgresql au Serveur Wildfly:
<br/>  • Connection au serveur Wilfly 127.0.0.1:8080, switch sur la console http://localhost:9990/console
Deployment -> add -> Upload a new deployement, choisir le driver .jar	 
Ensuite Configuration -> Subsystems -> Datasources -> Non-XA -> add -> Choose Datasource : PostgreSQL datasource

Create Datasource: name: PostgresDS
			  <br/>JNDI Name: java:/PostgresDS

JDBC Driver -> Detected driver -> postgresql-9.4.1212.jar

Connection URL : jdbc:postgresql://127.0.0.1:5432/postgres

Log pour postgres: name: postgres 
			  <br/>password: Bochimans1979